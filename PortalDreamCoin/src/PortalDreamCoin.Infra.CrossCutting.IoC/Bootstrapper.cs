﻿using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;
using PortalDreamCoin.Domain.Service;
using PortalDreamCoin.Infra.Data.Context;
using PortalDreamCoin.Infra.Data.Interface;
using PortalDreamCoin.Infra.Data.Repository;
using PortalDreamCoin.Infra.Data.UoW;
using SimpleInjector;

namespace PortalDreamCoin.Infra.CrossCutting.IoC
{
    public class Bootstrapper
    {
        public static Container MyContainer { get; set; }

        public static void Register(Container container)
        {
            // Lifestyle.Transient => Uma instancia para cada solicitacao;
            // Lifestyle.Singleton => Uma instancia unica para a classe;
            // Lifestyle.Scoped => Uma instancia unica para o request;

            // Domain Service
            container.Register<IAprovacaoService, AprovacaoService>(Lifestyle.Scoped);
            container.Register<IArtefatoSegurancaService, ArtefatoSegurancaService>(Lifestyle.Scoped);
            container.Register<IClienteService, ClienteService>(Lifestyle.Scoped);
            container.Register<IContaBancariaService, ContaBancariaService>(Lifestyle.Scoped);
            container.Register<IContaCorretagemService, ContaCorretagemService>(Lifestyle.Scoped);
            container.Register<IDepositoService, DepositoService>(Lifestyle.Scoped);
            container.Register<IEnderecoCarteiraVirtualService, EnderecoCarteiraVirtualService>(Lifestyle.Scoped);
            container.Register<IFuncionarioService, FuncionarioService>(Lifestyle.Scoped);
            container.Register<IOrdemService, OrdemService>(Lifestyle.Scoped);
            container.Register<IPerfilSegurancaService, PerfilSegurancaService>(Lifestyle.Scoped);
            container.Register<ISaqueService, SaqueService>(Lifestyle.Scoped);
            container.Register<ITipoMoedaService, TipoMoedaService>(Lifestyle.Scoped);
            container.Register<ITransferenciaService, TransferenciaService>(Lifestyle.Scoped);


            // Infra Repo
            container.Register<IAprovacaoRepository, AprovacaoRepository>(Lifestyle.Scoped);
            container.Register<IArtefatoSegurancaRepository, ArtefatoSegurancaRepository>(Lifestyle.Scoped);
            container.Register<IClienteRepository, ClienteRepository>(Lifestyle.Scoped);
            container.Register<IContaBancariaRepository, ContaBancariaRepository>(Lifestyle.Scoped);
            container.Register<IContaCorretagemRepository, ContaCorretagemRepository>(Lifestyle.Scoped);
            container.Register<IDepositoRepository, DepositoRepository>(Lifestyle.Scoped);
            container.Register<IEnderecoCarteiraVirtualRepository, EnderecoCarteiraVirtualRepository>(Lifestyle.Scoped);
            container.Register<IFuncionarioRepository, FuncionarioRepository>(Lifestyle.Scoped);
            container.Register<IOrdemRepository, OrdemRepository>(Lifestyle.Scoped);
            container.Register<IPerfilSegurancaRepository, PerfilSegurancaRepository>(Lifestyle.Scoped);
            container.Register<ISaqueRepository, SaqueRepository>(Lifestyle.Scoped);
            container.Register<ITipoMoedaRepository, TipoMoedaRepository>(Lifestyle.Scoped);
            container.Register<ITransferenciaRepository, TransferenciaRepository>(Lifestyle.Scoped);


            //Infra Dados
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.Register<DreamCoinContext>(Lifestyle.Scoped);

        }
    }
}