﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Infra.Data.Context;
using PortalDreamCoin.Infra.Data.Interface;

namespace PortalDreamCoin.Infra.Data.Repository
{
    public class DepositoRepository : RepositoryBase<Deposito>, IDepositoRepository
    {
        public DepositoRepository(DreamCoinContext context, IUnitOfWork unitOfWork) : base(context, unitOfWork)
        {
        }
    }
}