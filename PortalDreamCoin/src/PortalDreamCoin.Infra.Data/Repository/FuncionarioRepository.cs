﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Infra.Data.Context;
using PortalDreamCoin.Infra.Data.Interface;

namespace PortalDreamCoin.Infra.Data.Repository
{
    public class FuncionarioRepository : RepositoryBase<Funcionario>, IFuncionarioRepository
    {
        public FuncionarioRepository(DreamCoinContext context, IUnitOfWork unitOfWork) : base(context, unitOfWork)
        {
        }
    }
}