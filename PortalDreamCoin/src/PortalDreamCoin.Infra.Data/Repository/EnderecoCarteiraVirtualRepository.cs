﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Infra.Data.Context;
using PortalDreamCoin.Infra.Data.Interface;

namespace PortalDreamCoin.Infra.Data.Repository
{
    public class EnderecoCarteiraVirtualRepository : RepositoryBase<EnderecoCarteiraVirtual>, IEnderecoCarteiraVirtualRepository
    {
        public EnderecoCarteiraVirtualRepository(DreamCoinContext context, IUnitOfWork unitOfWork) : base(context, unitOfWork)
        {
        }
    }
}