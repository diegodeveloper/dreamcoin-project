﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Infra.Data.Context;
using PortalDreamCoin.Infra.Data.Interface;

namespace PortalDreamCoin.Infra.Data.Repository
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly DreamCoinContext _context;
        protected readonly DbSet<TEntity> _dbSet;
        private readonly IUnitOfWork _unitOfWork;

        public RepositoryBase(DreamCoinContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
            this._unitOfWork = unitOfWork;
        }

        protected void Commit()
        {
            _unitOfWork.Commit();
        }

        public void Adicionar(TEntity obj)
        {
            _dbSet.Add(obj);
            Commit();
        }

        public void Atualizar(TEntity obj)
        {
            _dbSet.AddOrUpdate(obj);
            Commit();
        }

        public void Deletar(Guid id)
        {
            var obj = ObterPorId(id, false);
            _context.Entry(obj).State = EntityState.Deleted;
            Commit();
        }

        public TEntity ObterPorId(Guid id, bool @readonly = true)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<TEntity> ObterTodos(bool @readonly = true)
        {
            return @readonly
                ? _dbSet.AsNoTracking().ToList()
                : _dbSet.ToList();
        }

        public IEnumerable<TEntity> Buscar(Expression<Func<TEntity, bool>> predicate, bool @readonly = true)
        {
            return @readonly
                ? _dbSet.Where(predicate).AsNoTracking()
                : _dbSet.Where(predicate);
        }
    }
}