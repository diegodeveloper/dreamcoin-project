﻿namespace PortalDreamCoin.Infra.Data.Interface
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}