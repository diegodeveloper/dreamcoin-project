﻿using PortalDreamCoin.Domain.Entities;

namespace PortalDreamCoin.Domain.Interface.Repository
{
    public interface ITipoMoedaRepository : IRepositoryBase<TipoMoeda>
    {
        
    }
}