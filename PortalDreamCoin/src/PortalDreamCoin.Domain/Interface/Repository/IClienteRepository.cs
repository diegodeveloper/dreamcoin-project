﻿using PortalDreamCoin.Domain.Entities;

namespace PortalDreamCoin.Domain.Interface.Repository
{
    public interface IClienteRepository : IRepositoryBase<Cliente>
    {
        
    }
}