﻿using PortalDreamCoin.Domain.Entities;

namespace PortalDreamCoin.Domain.Interface.Service
{
    public interface IAprovacaoService : IServiceBase<Aprovacao>
    {
        
    }
}