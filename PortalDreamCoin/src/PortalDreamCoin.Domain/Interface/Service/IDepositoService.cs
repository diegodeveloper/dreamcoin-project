﻿using PortalDreamCoin.Domain.Entities;

namespace PortalDreamCoin.Domain.Interface.Service
{
    public interface IDepositoService : IServiceBase<Deposito>
    {
        
    }
}