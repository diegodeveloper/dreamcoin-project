﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace PortalDreamCoin.Domain.Interface.Service
{
    public interface IServiceBase<TEntity> where TEntity : class
    {
        void Adicionar(TEntity obj);

        void Atualizar(TEntity obj);

        void Deletar(Guid id);

        TEntity ObterPorId(Guid id, bool @readonly = true);

        IEnumerable<TEntity> ObterTodos(bool @readonly = true);

        IEnumerable<TEntity> Buscar(Expression<Func<TEntity, bool>> predicate, bool @readonly = true);
    }
}