﻿using PortalDreamCoin.Domain.Entities;

namespace PortalDreamCoin.Domain.Interface.Service
{
    public interface IContaBancariaService : IServiceBase<ContaBancaria>
    {
        
    }
}