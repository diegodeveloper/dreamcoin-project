﻿using PortalDreamCoin.Domain.Entities;

namespace PortalDreamCoin.Domain.Interface.Service
{
    public interface ITransferenciaService : IServiceBase<Transferencia>
    {
        
    }
}