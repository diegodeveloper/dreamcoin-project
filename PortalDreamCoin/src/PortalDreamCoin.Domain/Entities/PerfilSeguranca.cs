﻿using System;

namespace PortalDreamCoin.Domain.Entities
{
    public class PerfilSeguranca
    {
        public Guid Id { get; set; }

        public int Senha { get; set; }

        public virtual  Cliente Cliente { get; set; }
    }
}