﻿using System;
using System.Collections.Generic;
using PortalDreamCoin.Domain.ValuesObjects;

namespace PortalDreamCoin.Domain.Entities
{
    public class Cliente
    {
        public Guid Id { get; set; }

        public string Cpf { get; set; }

        public string Cnpj { get; set; }

        public DateTime DataNascimento { get; set; }

        public string Email { get; set; }

        public Cpf ObjCpf { get; set; }

        public Cnpj ObjCnpj { get; set; }

        public virtual ArtefatoSeguranca ArtefatoSeguranca { get; set; }

        public virtual PerfilSeguranca PerfilSeguranca { get; set; }

        public virtual ICollection<ContaBancaria> ContaBancariaCollection { get; set; }


    }
}