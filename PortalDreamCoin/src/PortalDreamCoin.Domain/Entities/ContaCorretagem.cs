﻿using System;
using System.Collections.Generic;

namespace PortalDreamCoin.Domain.Entities
{
    public class ContaCorretagem
    {
        public Guid Id { get; set; }

        public decimal Saldo { get; set; }

        public virtual ICollection<EnderecoCarteiraVirtual> EnderecoCarteiraVirtualsCollection { get; set; }

        public virtual ICollection<Transferencia> TransferenciaCollection { get; set; }
    }
}