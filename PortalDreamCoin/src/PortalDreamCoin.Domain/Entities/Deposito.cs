﻿using System;

namespace PortalDreamCoin.Domain.Entities
{
    public class Deposito
    {
        public Guid Id { get; set; }

        public int BancoDestino { get; set; }

        public byte[] ComprovanteDeposito { get; set; }

        public string Descricao { get; set; }

        public bool Status { get; set; }

        public int TipoDesito { get; set; }

        public decimal Valor { get; set; }

        public decimal ValorCreditado { get; set; }

        public virtual Ordem Ordem { get; set; }
    }
}