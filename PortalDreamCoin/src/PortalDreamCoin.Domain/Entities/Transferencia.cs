﻿using System;

namespace PortalDreamCoin.Domain.Entities
{
    public class Transferencia
    {
        public Guid Id { get; set; }

        public string Descricao { get; set; }

        public string Endereco { get; set; }

        public decimal Quantidade { get; set; }

        public string TipoTaxaMineradores { get; set; }

        public decimal TotalDebitado { get; set; }

        public virtual ContaCorretagem ContaCorretagem { get; set; }
    }
}