﻿using System;

namespace PortalDreamCoin.Domain.Entities
{
    public class EnderecoCarteiraVirtual
    {
        public Guid Id { get; set; }

        public string Endereco { get; set; }

        public virtual TipoMoeda TipoMoeda { get; set; }

        public virtual ContaCorretagem ContaCorretagem { get; set; }

    }
}