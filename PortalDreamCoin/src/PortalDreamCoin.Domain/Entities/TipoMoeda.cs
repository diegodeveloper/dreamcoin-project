﻿using System;

namespace PortalDreamCoin.Domain.Entities
{
    public class TipoMoeda
    {
        public Guid Id { get; set; }

        public string Nome { get; set; }

    }
}