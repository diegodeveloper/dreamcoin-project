﻿using System;

namespace PortalDreamCoin.Domain.Entities
{
    public class ArtefatoSeguranca
    {
        public Guid Id { get; set; }

        public int Pin { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}