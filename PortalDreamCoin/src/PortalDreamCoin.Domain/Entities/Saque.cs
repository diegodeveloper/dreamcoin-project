﻿using System;

namespace PortalDreamCoin.Domain.Entities
{
    public class Saque
    {
        public Guid Id { get; set; }

        public decimal Comissao { get; set; }

        public string Decisao { get; set; }

        public bool Status { get; set; }

        public decimal Valor { get; set; }

        public decimal ValorSaque { get; set; }

        public virtual Ordem Ordem { get; set; }

        public virtual ContaBancaria ContaBancaria { get; set; }
    }
}