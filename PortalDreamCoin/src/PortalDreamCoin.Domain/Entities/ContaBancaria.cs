﻿using System;
using System.Collections.Generic;

namespace PortalDreamCoin.Domain.Entities
{
    public class ContaBancaria
    {
        public Guid Id { get; set; }

        public string Agencia { get; set; }

        public string Banco { get; set; }

        public string Numero { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual ICollection<Saque> SaqueCollection { get; set; }
    }
}