﻿using System;

namespace PortalDreamCoin.Domain.Entities
{
    public class Aprovacao
    {
        public Guid Id { get; set; }

        public DateTime Data { get; set; }
    }
}