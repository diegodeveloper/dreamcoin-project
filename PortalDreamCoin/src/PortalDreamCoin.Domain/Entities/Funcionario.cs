﻿using System;
using PortalDreamCoin.Domain.ValuesObjects;

namespace PortalDreamCoin.Domain.Entities
{
    public class Funcionario
    {
        public Guid Id { get; set; }

        public string Cpf { get; set; }

        public string Nome { get; set; }

        public Cpf ObjCpf { get; set; } 
    }
}