﻿using System;
using System.Collections.Generic;

namespace PortalDreamCoin.Domain.Entities
{
    public class Ordem
    {
        public Guid Id { get; set; }

        public DateTime DataExecucao { get; set; }

        public decimal Quantidade { get; set; }

        public int TipoOrdem { get; set; }

        public decimal Valor { get; set; }

        public virtual ICollection<Saque> SaquesCollection { get; set; }

        public virtual ICollection<Deposito> DepositoCollection { get; set; }
    }
}
