﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;

namespace PortalDreamCoin.Domain.Service
{
    public class EnderecoCarteiraVirtualService : ServiceBase<EnderecoCarteiraVirtual>, IEnderecoCarteiraVirtualService
    {
        private readonly IEnderecoCarteiraVirtualRepository _repository;

        public EnderecoCarteiraVirtualService(IEnderecoCarteiraVirtualRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}