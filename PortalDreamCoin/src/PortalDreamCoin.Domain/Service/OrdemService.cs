﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;

namespace PortalDreamCoin.Domain.Service
{
    public class OrdemService : ServiceBase<Ordem>, IOrdemService
    {
        private readonly IOrdemRepository _repository;

        public OrdemService(IOrdemRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}