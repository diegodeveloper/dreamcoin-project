﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;

namespace PortalDreamCoin.Domain.Service
{
    public class TransferenciaService : ServiceBase<Transferencia>, ITransferenciaService
    {
        private readonly ITransferenciaRepository _repository;

        public TransferenciaService(ITransferenciaRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}