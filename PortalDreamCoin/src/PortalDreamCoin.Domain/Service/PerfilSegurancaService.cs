﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;

namespace PortalDreamCoin.Domain.Service
{
    public class PerfilSegurancaService : ServiceBase<PerfilSeguranca>, IPerfilSegurancaService
    {
        private readonly IPerfilSegurancaRepository _repository;

        public PerfilSegurancaService(IPerfilSegurancaRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}