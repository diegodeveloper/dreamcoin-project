﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;

namespace PortalDreamCoin.Domain.Service
{
    public class AprovacaoService : ServiceBase<Aprovacao>, IAprovacaoService
    {
        private readonly IAprovacaoRepository _repository;

        public AprovacaoService(IAprovacaoRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}