﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;

namespace PortalDreamCoin.Domain.Service
{
    public class ArtefatoSegurancaService : ServiceBase<ArtefatoSeguranca>, IArtefatoSegurancaService
    {
        private readonly IArtefatoSegurancaRepository _repository;

        public ArtefatoSegurancaService(IArtefatoSegurancaRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}