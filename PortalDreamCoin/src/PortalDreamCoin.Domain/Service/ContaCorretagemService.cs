﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;

namespace PortalDreamCoin.Domain.Service
{
    public class ContaCorretagemService : ServiceBase<ContaCorretagem>, IContaCorretagemService
    {
        private readonly IContaCorretagemRepository _repository;

        public ContaCorretagemService(IContaCorretagemRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}