﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;

namespace PortalDreamCoin.Domain.Service
{
    public class SaqueService : ServiceBase<Saque>, ISaqueService
    {
        private readonly ISaqueRepository _repository;

        public SaqueService(ISaqueRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}