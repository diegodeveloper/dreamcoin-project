﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;

namespace PortalDreamCoin.Domain.Service
{
    public class ClienteService : ServiceBase<Cliente>, IClienteService
    {
        private readonly IClienteRepository _repository;

        public ClienteService(IClienteRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}