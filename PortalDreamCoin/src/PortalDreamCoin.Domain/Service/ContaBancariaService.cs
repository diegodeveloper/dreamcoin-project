﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;

namespace PortalDreamCoin.Domain.Service
{
    public class ContaBancariaService : ServiceBase<ContaBancaria>, IContaBancariaService
    {
        private readonly IContaBancariaRepository _repository;

        public ContaBancariaService(IContaBancariaRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}