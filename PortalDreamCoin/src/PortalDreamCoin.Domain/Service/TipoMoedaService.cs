﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;

namespace PortalDreamCoin.Domain.Service
{
    public class TipoMoedaService : ServiceBase<TipoMoeda>, ITipoMoedaService
    {
        private readonly ITipoMoedaRepository _repository;

        public TipoMoedaService(ITipoMoedaRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}