﻿using PortalDreamCoin.Domain.Entities;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;

namespace PortalDreamCoin.Domain.Service
{
    public class DepositoService : ServiceBase<Deposito>, IDepositoService
    {
        private readonly IDepositoRepository _repository;

        public DepositoService(IDepositoRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}