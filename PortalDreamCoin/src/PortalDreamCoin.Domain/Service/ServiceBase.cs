﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using PortalDreamCoin.Domain.Interface.Repository;
using PortalDreamCoin.Domain.Interface.Service;

namespace PortalDreamCoin.Domain.Service
{
    public class ServiceBase<TEntity> : IServiceBase<TEntity> where TEntity : class
    {
        private readonly IRepositoryBase<TEntity> _repository;

        public ServiceBase(IRepositoryBase<TEntity> repository)
        {
            _repository = repository;
        }

        public void Adicionar(TEntity obj)
        {
            _repository.Adicionar(obj);
        }

        public void Atualizar(TEntity obj)
        {
            _repository.Atualizar(obj);
        }

        public void Deletar(Guid id)
        {
            _repository.Deletar(id);
        }

        public TEntity ObterPorId(Guid id, bool @readonly = true)
        {
            return _repository.ObterPorId(id, @readonly);
        }

        public IEnumerable<TEntity> ObterTodos(bool @readonly = true)
        {
            return _repository.ObterTodos(@readonly);
        }

        public IEnumerable<TEntity> Buscar(Expression<Func<TEntity, bool>> predicate, bool @readonly = true)
        {
            return _repository.Buscar(predicate, @readonly);
        }
    }
}