﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PortalDreamCoin.Cadastro.Application.AppServices;
using PortalDreamCoin.Cadastro.Application.Interfaces;
using PortalDreamCoin.Cadastro.Data.Context;
using PortalDreamCoin.Cadastro.Data.Interfaces;
using PortalDreamCoin.Cadastro.Data.Repository;
using PortalDreamCoin.Cadastro.Data.UoW;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Interfaces;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Service;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Interfaces;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Service;
using PortalDreamCoin.Cadastro.Infra.Identity.Configuration;
using PortalDreamCoin.Cadastro.Infra.Identity.Context;
using PortalDreamCoin.Cadastro.Infra.Identity.Model;
using PortalDreamCoin.Core.Domain.Events;
using PortalDreamCoin.Core.Domain.Handlers;
using PortalDreamCoin.Core.Domain.Interfaces;
using SimpleInjector;

namespace PortalDreamCoin.Cadastro.Infra.IoC
{
    public class BootStrapper
    {
        public static Container MyContainer { get; set; }

        public static void Register(Container container)
        {
            // Lifestyle.Transient => Uma instancia para cada solicitacao;
            // Lifestyle.Singleton => Uma instancia unica para a classe;
            // Lifestyle.Scoped => Uma instancia unica para o request;

            MyContainer = container;

            // APP
            container.Register<IPessoaFisicaAppService, PessoaFisicaAppService>(Lifestyle.Scoped);
            container.Register<IPessoaJuridicaAppService, PessoaJuridicaAppService>(Lifestyle.Scoped);

            // Domain
            container.Register<IPessoaFisicaService, PessoaFisicaService>(Lifestyle.Scoped);
            container.Register<IPessoaJuridicaService, PessoaJuridicaService>(Lifestyle.Scoped);

            // Infra Dados Repos
            container.Register<IPessoaFisicaRepository, PessoaFisicaRepository>(Lifestyle.Scoped);
            container.Register<IPessoaJuridicaRepository, PessoaJuridicaRepository>(Lifestyle.Scoped);

            // Infra Dados
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.Register<CadastroContext>(Lifestyle.Scoped);

            // Handlers
            container.Register<IHandler<DomainNotification>, DomainNotificationHandler>(Lifestyle.Scoped);
            //container.Register<IHandler<ClienteCadastradoEvent>, ClienteCadastradoHandler>(Lifestyle.Scoped);

            // Infra Core
            //container.Register<IEnvioEmail, EnvioEmail>(Lifestyle.Singleton);

            // Identity
            container.Register<ApplicationDbContext>(Lifestyle.Scoped);
            container.Register<IUserStore<ApplicationUser>>(() => new UserStore<ApplicationUser>(new ApplicationDbContext()), Lifestyle.Scoped);
            container.Register<IRoleStore<IdentityRole, string>>(() => new RoleStore<IdentityRole>(), Lifestyle.Scoped);
            container.Register<ApplicationRoleManager>(Lifestyle.Scoped);
            container.Register<ApplicationUserManager>(Lifestyle.Scoped);
            container.Register<ApplicationSignInManager>(Lifestyle.Scoped);
        }
    }
}