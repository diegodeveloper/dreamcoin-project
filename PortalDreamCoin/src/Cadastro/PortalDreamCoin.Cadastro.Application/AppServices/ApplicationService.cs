﻿using Microsoft.AspNet.Identity;
using PortalDreamCoin.Cadastro.Data.Interfaces;
using PortalDreamCoin.Core.Domain.Events;
using PortalDreamCoin.Core.Domain.Interfaces;

namespace PortalDreamCoin.Cadastro.Application.AppServices
{
    public class ApplicationService
    {
        private readonly IUnitOfWork _unitOfWork;
        protected readonly IHandler<DomainNotification> Notifications;

        public ApplicationService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.Notifications = DomainEvent.Container.GetInstance<IHandler<DomainNotification>>();
        }

        public bool Commit()
        {
            if (Notifications.HasNotifications())
                return false;

            _unitOfWork.Commit();
            return true;
        }
    }
}