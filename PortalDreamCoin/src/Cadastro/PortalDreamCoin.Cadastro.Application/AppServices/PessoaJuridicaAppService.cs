﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using PortalDreamCoin.Cadastro.Application.Adapters;
using PortalDreamCoin.Cadastro.Application.Interfaces;
using PortalDreamCoin.Cadastro.Data.Interfaces;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Entities;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Interfaces;
using PortalDreamCoin.Cadastro.Infra.Identity.Configuration;
using PortalDreamCoin.Cadastro.Infra.Identity.Model;
using PortalDreamCoin.Core.Domain.Events;

namespace PortalDreamCoin.Cadastro.Application.AppServices
{
    public class PessoaJuridicaAppService : ApplicationService, IPessoaJuridicaAppService
    {
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly ApplicationUserManager _userManager;
        public PessoaJuridicaAppService(IUnitOfWork unitOfWork, IPessoaJuridicaService pessoaJuridicaService, ApplicationUserManager userManager) : base(unitOfWork)
        {
            _pessoaJuridicaService = pessoaJuridicaService;
            _userManager = userManager;
        }

        public PessoaJuridica Adicionar(PessoaJuridica pessoaJuridica)
        {
            if (!Notifications.HasNotifications())
            {
                _pessoaJuridicaService.Adicionar(pessoaJuridica);
            }

            return pessoaJuridica;
        }

        public PessoaJuridica ObterPorId(Guid id)
        {
            throw new NotImplementedException();
        }

        public IdentityResult AdicionarIdentidade(RegisterViewModel register)
        {
            var manager = _userManager;

            var user = new ApplicationUser { UserName = register.Email, Email = register.Email };
            var result = manager.Create(user, register.Password);

            if (result.Succeeded)
            {
                var pessoaJuridica = PessoaJuridicaAdapter.ToDomainModel(Guid.Parse(user.Id), register);
                Adicionar(pessoaJuridica);

                if (Commit())
                {
                    //Enviar Mensage/Email para o cliente cadastrado
                    //DomainEvent.Raise(new ClienteCadastradoEvent(cliente));
                }
                else
                {
                    manager.Delete(user);
                    return new IdentityResult(Notifications.ToString());
                }

            }
            else
            {
                var errosBr = new List<string>();
                var notificationList = new List<DomainNotification>();

                foreach (var erro in result.Errors)
                {
                    string erroBr;
                    if (erro.Contains("Passwords must have at least one digit ('0'-'9')."))
                    {
                        erroBr = "A senha precisa ter ao menos um dígito";
                        notificationList.Add(new DomainNotification("IdentityValidation", erroBr));
                        errosBr.Add(erroBr);
                    }
                    if (erro.Contains("Passwords must have at least one non letter or digit character."))
                    {
                        erroBr = "A senha precisa ter ao menos um caractere especial (@, #, etc...)";
                        notificationList.Add(new DomainNotification("IdentityValidation", erroBr));
                        errosBr.Add(erroBr);
                    }
                    if (erro.Contains("Passwords must have at least one lowercase ('a'-'z')."))
                    {
                        erroBr = "A senha precisa ter ao menos uma letra em minúsculo";
                        notificationList.Add(new DomainNotification("IdentityValidation", erroBr));
                        errosBr.Add(erroBr);
                    }
                    if (erro.Contains("Passwords must have at least one uppercase ('A'-'Z')."))
                    {
                        erroBr = "A senha precisa ter ao menos uma letra em maiúsculo";
                        notificationList.Add(new DomainNotification("IdentityValidation", erroBr));
                        errosBr.Add(erroBr);
                    }
                    if (erro.Contains("Name " + register.Email + " is already taken"))
                    {
                        erroBr = "E-mail já registrado, esqueceu sua senha?";
                        notificationList.Add(new DomainNotification("IdentityValidation", erroBr));
                        errosBr.Add(erroBr);
                    }
                }
                notificationList.ForEach(DomainEvent.Raise);
                result = new IdentityResult(errosBr);
            }

            return result;
        }
    }
}