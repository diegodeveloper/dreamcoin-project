﻿using System;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Entities;
using PortalDreamCoin.Cadastro.Infra.Identity.Model;

namespace PortalDreamCoin.Cadastro.Application.Adapters
{
    public class PessoaJuridicaAdapter
    {
        public static PessoaJuridica ToDomainModel(Guid id, RegisterViewModel registerViewModel)
        {
            var pessoaJuridica = new PessoaJuridica(id, registerViewModel.Email, registerViewModel.Nome, registerViewModel.CNPJ);
            return pessoaJuridica;
        }
    }
}