﻿using System;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Entities;
using PortalDreamCoin.Cadastro.Infra.Identity.Model;

namespace PortalDreamCoin.Cadastro.Application.Adapters
{
    public class PessoaFisicaAdapter
    {
        public static PessoaFisica ToDomainModel(Guid id, RegisterViewModel registerViewModel)
        {
            var pessoaFisica = new PessoaFisica(id,registerViewModel.Email,registerViewModel.Nome,registerViewModel.CPF);
            return pessoaFisica;
        }
    }
}