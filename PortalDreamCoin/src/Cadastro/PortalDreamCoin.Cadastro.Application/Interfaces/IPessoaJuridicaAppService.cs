﻿using System;
using Microsoft.AspNet.Identity;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Entities;
using PortalDreamCoin.Cadastro.Infra.Identity.Model;

namespace PortalDreamCoin.Cadastro.Application.Interfaces
{
    public interface IPessoaJuridicaAppService
    {
        PessoaJuridica Adicionar(PessoaJuridica pessoaJuridica);
        PessoaJuridica ObterPorId(Guid id);
        IdentityResult AdicionarIdentidade(RegisterViewModel register);
    }
}