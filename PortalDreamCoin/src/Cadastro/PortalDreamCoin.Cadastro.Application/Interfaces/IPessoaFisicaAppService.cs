﻿using System;
using Microsoft.AspNet.Identity;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Entities;
using PortalDreamCoin.Cadastro.Infra.Identity.Model;

namespace PortalDreamCoin.Cadastro.Application.Interfaces
{
    public interface IPessoaFisicaAppService
    {
        PessoaFisica Adicionar(PessoaFisica cliente);
        PessoaFisica ObterPorId(Guid id);
        IdentityResult AdicionarIdentidade(RegisterViewModel register);
    }
}