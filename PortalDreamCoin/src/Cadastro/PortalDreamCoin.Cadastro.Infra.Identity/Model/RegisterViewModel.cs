using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PortalDreamCoin.Cadastro.Infra.Identity.Model
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Preencha o campo Nome")]
        [MaxLength(150, ErrorMessage = "M�ximo 150 caracteres")]
        [MinLength(2, ErrorMessage = "M�nimo 2 caracteres")]
        public string Nome { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Confirm Email")]
        [Compare("Email", ErrorMessage = "O email est� diferente.")]
        public string ConfirmEmail { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "O {0} deve ter pelo menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirme password")]
        [Compare("Password", ErrorMessage = "As senhas s�o diferentes.")]
        public string ConfirmPassword { get; set; }

        [MaxLength(11, ErrorMessage = "M�ximo 11 caracteres")]
        [DisplayName("CPF")]
        public string CPF { get; set; }

        [MaxLength(14, ErrorMessage = "M�ximo 14 caracteres")]
        [DisplayName("CNPJ")]
        public string CNPJ { get; set; }
    }
}