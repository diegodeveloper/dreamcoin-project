﻿namespace PortalDreamCoin.Cadastro.Infra.Identity.Model
{
    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }
}