﻿using DomainValidation.Interfaces.Specification;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Interfaces;

namespace PortalDreamCoin.Cadastro.Domain.PessoaFisica.Specification
{
    public class PessoaFisicaDevePossuirCpfUnicoSpecification : ISpecification<Entities.PessoaFisica>
    {
        private readonly IPessoaFisicaRepository _repository;

        public PessoaFisicaDevePossuirCpfUnicoSpecification(IPessoaFisicaRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(Entities.PessoaFisica pessoa)
        {
            return _repository.ObterPorCpf(pessoa.Cpf.GetCpfCompleto()) == null;
        }
    }
}