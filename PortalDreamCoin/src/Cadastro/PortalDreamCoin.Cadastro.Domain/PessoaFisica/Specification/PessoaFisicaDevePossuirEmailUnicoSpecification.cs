﻿using DomainValidation.Interfaces.Specification;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Interfaces;

namespace PortalDreamCoin.Cadastro.Domain.PessoaFisica.Specification
{
    public class PessoaFisicaDevePossuirEmailUnicoSpecification: ISpecification<Entities.PessoaFisica>
    {
        private readonly IPessoaFisicaRepository _repository;

        public PessoaFisicaDevePossuirEmailUnicoSpecification(IPessoaFisicaRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(Entities.PessoaFisica pessoa)
        {
            return _repository.ObterPorEmail(pessoa.Email.Endereco) == null;
        }
    }
}