﻿using PortalDreamCoin.Core.Domain.ValuesObjects;

namespace PortalDreamCoin.Cadastro.Domain.PessoaFisica.Entities
{
    public static class PessoaFisicaScopes
    {
        public static bool DefinirCpfClienteScopeEhValido(this PessoaFisica pessoaFisica, Cpf cpf)
        {
            return AssertionConcern.IsSatisfiedBy
            (
                AssertionConcern.AssertFixedLength(cpf.GetCpfCompleto(), Cpf.ValorMaxCpf, "CPF em tamanho incorreto"),
                AssertionConcern.AssertNotNullOrEmpty(cpf.Codigo, "O CPF obrigatória"),
                AssertionConcern.AssertTrue(Cpf.Validar(cpf.Codigo), "CPF em formato inválido")
            );
        }
    }
}