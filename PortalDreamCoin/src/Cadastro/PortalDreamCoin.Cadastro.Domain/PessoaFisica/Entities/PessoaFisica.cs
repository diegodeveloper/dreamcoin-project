﻿using System;
using PortalDreamCoin.Cadastro.Domain.Common.Entities;
using PortalDreamCoin.Core.Domain.ValuesObjects;

namespace PortalDreamCoin.Cadastro.Domain.PessoaFisica.Entities
{
    public class PessoaFisica : Pessoa
    {
        public PessoaFisica(Guid id, string email, string nome, string cpf) : base(id,email)
        {
            Nome = nome;
            DefinirCpf(cpf);
        }

        protected PessoaFisica()
        {
            
        }

        public string Nome { get; private set; }

        public Cpf Cpf { get; private set; }

        public void DefinirCpf(string cpf)
        {
            var myCpf = new Cpf(cpf);

            if (this.DefinirCpfClienteScopeEhValido(myCpf))
                Cpf = myCpf;
        }
    }
}