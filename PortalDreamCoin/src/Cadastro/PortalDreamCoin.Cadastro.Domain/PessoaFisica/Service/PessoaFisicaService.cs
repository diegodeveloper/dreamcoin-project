﻿using PortalDreamCoin.Cadastro.Domain.Common.Service;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Interfaces;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Validation;

namespace PortalDreamCoin.Cadastro.Domain.PessoaFisica.Service
{
    public class PessoaFisicaService : ServiceBase<Entities.PessoaFisica>, IPessoaFisicaService
    {
        private readonly IPessoaFisicaRepository _repository;

        public PessoaFisicaService(IPessoaFisicaRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public override Entities.PessoaFisica Adicionar(Entities.PessoaFisica pessoa)
        {
            if (PossuiConformidade(new PessoaFisicaAptoParaCadastroValidation(_repository).Validate(pessoa)))
                _repository.Adicionar(pessoa);

            return pessoa;
        }
    }
}