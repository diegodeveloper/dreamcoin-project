﻿using PortalDreamCoin.Cadastro.Domain.Common.Interfaces;

namespace PortalDreamCoin.Cadastro.Domain.PessoaFisica.Interfaces
{
    public interface IPessoaFisicaRepository : IRepositoryBase<Entities.PessoaFisica>
    {
        Entities.PessoaFisica ObterPorCpf(string cpf);
        Entities.PessoaFisica ObterPorEmail(string email);
    }
}