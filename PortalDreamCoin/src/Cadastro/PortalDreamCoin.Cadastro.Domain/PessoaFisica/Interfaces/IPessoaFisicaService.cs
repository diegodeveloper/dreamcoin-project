﻿using PortalDreamCoin.Cadastro.Domain.Common.Interfaces;

namespace PortalDreamCoin.Cadastro.Domain.PessoaFisica.Interfaces
{
    public interface IPessoaFisicaService : IServiceBase<Entities.PessoaFisica>
    {
        
    }
}