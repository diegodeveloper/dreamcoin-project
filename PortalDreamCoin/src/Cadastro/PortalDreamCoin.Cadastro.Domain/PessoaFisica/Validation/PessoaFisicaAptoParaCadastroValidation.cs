﻿using DomainValidation.Validation;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Interfaces;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Specification;

namespace PortalDreamCoin.Cadastro.Domain.PessoaFisica.Validation
{
    public class PessoaFisicaAptoParaCadastroValidation : Validator<Entities.PessoaFisica>
    {
        public PessoaFisicaAptoParaCadastroValidation(IPessoaFisicaRepository repository)
        {
            var cpfDuplicado = new PessoaFisicaDevePossuirCpfUnicoSpecification(repository);
            var emailDuplicado = new PessoaFisicaDevePossuirEmailUnicoSpecification(repository);

            base.Add("cpfDuplicado", new Rule<Entities.PessoaFisica>(cpfDuplicado, "CPF já cadastrado!"));
            base.Add("emailDuplicado", new Rule<Entities.PessoaFisica>(emailDuplicado, "E-mail já cadastrado!"));
        }
    }
}