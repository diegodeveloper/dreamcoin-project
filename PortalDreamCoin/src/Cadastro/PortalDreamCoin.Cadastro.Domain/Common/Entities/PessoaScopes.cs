﻿using PortalDreamCoin.Core.Domain.ValuesObjects;

namespace PortalDreamCoin.Cadastro.Domain.Common.Entities
{
    public static class PessoaScopes
    {
        public static bool DefinirEmailClienteScopeEhValido(this Pessoa pessoa, Email email)
        {
            return AssertionConcern.IsSatisfiedBy
            (
                AssertionConcern.AssertLength(email.Endereco, Email.EnderecoMinLength, Email.EnderecoMaxLength,
                    "E-mail em tamanho incorreto"),
                AssertionConcern.AssertNotNullOrEmpty(email.Endereco, "O e-mail obrigatória"),
                AssertionConcern.AssertTrue(Email.IsValid(email.Endereco), "E-mail em formato inválido")
            );
        }
    }
}