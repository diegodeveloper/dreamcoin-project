﻿using System;
using DomainValidation.Validation;
using PortalDreamCoin.Core.Domain.ValuesObjects;

namespace PortalDreamCoin.Cadastro.Domain.Common.Entities
{
    public class Pessoa
    {
        protected Pessoa()
        {
            
        }

        public Pessoa(Guid id, string email)
        {
            Id = id;
            Ativo = false;
            DefinirEmail(email);
        }
        public Guid  Id { get; private set; }

        public Email Email { get; private set; }

        public bool Ativo { get; private set; }

        public ValidationResult ValidationResult { get; set; }

        public void DefinirEmail(string email)
        {
            var myEmail = new Email(email);

            if (this.DefinirEmailClienteScopeEhValido(myEmail))
                Email = myEmail;
        }

        public void Ativar()
        {
            Ativo = true;
        }

        public void Inativar()
        {
            Ativo = false;
        }

        #region Constantes

        public const int SenhaMinLength = 6;
        public const int SenhaMaxLength = 30;

        #endregion
    }
}