﻿using System;

namespace PortalDreamCoin.Cadastro.Domain.Common.Interfaces
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        void Adicionar(TEntity obj);

        void Atualizar(TEntity obj);

        TEntity ObterPorId(Guid id);
    }
}