﻿namespace PortalDreamCoin.Cadastro.Domain.Common.Interfaces
{
    public interface IServiceBase<TEntity> where TEntity : class
    {
        TEntity Adicionar(TEntity obj);
    }
}