﻿using System.Linq;
using DomainValidation.Validation;
using PortalDreamCoin.Cadastro.Domain.Common.Interfaces;
using PortalDreamCoin.Core.Domain.Events;

namespace PortalDreamCoin.Cadastro.Domain.Common.Service
{
    public class ServiceBase<TEntity> : IServiceBase<TEntity> where TEntity : class
    {
        private readonly IRepositoryBase<TEntity> _repository;

        public ServiceBase(IRepositoryBase<TEntity> repository)
        {
            _repository = repository;
        }

        public virtual TEntity Adicionar(TEntity obj)
        {
            _repository.Adicionar(obj);
            return obj;
        }

        protected static bool PossuiConformidade(ValidationResult validationResult)
        {
            if (validationResult == null) return true;
            var notifications = validationResult.Erros.Select(validationError => new DomainNotification(validationError.ToString(), validationError.Message)).ToList();
            if (!notifications.Any()) return true;
            notifications.ToList().ForEach(DomainEvent.Raise);
            return false;
        }
    }
}