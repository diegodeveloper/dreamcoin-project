﻿using PortalDreamCoin.Cadastro.Domain.Common.Service;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Interfaces;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Validation;

namespace PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Service
{
    public class PessoaJuridicaService : ServiceBase<Entities.PessoaJuridica>, IPessoaJuridicaService
    {
        private readonly IPessoaJuridicaRepository _repository;

        public PessoaJuridicaService(IPessoaJuridicaRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public override Entities.PessoaJuridica Adicionar(Entities.PessoaJuridica pessoa)
        {
            if (PossuiConformidade(new PessoaJuridicaAptoParaCadastroValidation(_repository).Validate(pessoa)))
                _repository.Adicionar(pessoa);

            return pessoa;
        }
    }
}