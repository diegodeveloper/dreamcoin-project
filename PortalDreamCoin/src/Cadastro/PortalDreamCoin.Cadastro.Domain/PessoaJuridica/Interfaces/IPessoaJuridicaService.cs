﻿using PortalDreamCoin.Cadastro.Domain.Common.Interfaces;

namespace PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Interfaces
{
    public interface IPessoaJuridicaService : IServiceBase<Entities.PessoaJuridica>
    {
        
    }
}