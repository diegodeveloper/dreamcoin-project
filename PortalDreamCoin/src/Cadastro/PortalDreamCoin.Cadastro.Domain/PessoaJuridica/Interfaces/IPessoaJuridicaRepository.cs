﻿using PortalDreamCoin.Cadastro.Domain.Common.Interfaces;

namespace PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Interfaces
{
    public interface IPessoaJuridicaRepository : IRepositoryBase<Entities.PessoaJuridica>
    {
        Entities.PessoaJuridica ObterPorCnpj(string cnpj);
        Entities.PessoaJuridica ObterPorEmail(string email);
    }
}