﻿using System;
using PortalDreamCoin.Cadastro.Domain.Common.Entities;
using PortalDreamCoin.Core.Domain.ValuesObjects;

namespace PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Entities
{
    public class PessoaJuridica : Pessoa
    {
        public PessoaJuridica(Guid id, string email, string razaoSocial, string cnpj) : base(id, email)
        {
            RazaoSocial = razaoSocial;
            DefinirCnpj(cnpj);
        }

        public string RazaoSocial { get; private set; }

        public Cnpj Cnpj { get; private set; }

        public void DefinirCnpj(string cnpj)
        {
            var myCnpj = new Cnpj(cnpj);

            if (this.DefinirCnpjClienteScopeEhValido(myCnpj))
            Cnpj = myCnpj;
        }
    }
}