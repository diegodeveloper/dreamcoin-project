﻿using PortalDreamCoin.Core.Domain.ValuesObjects;

namespace PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Entities
{
    public static class PessoaJuridicaScopes
    {
        public static bool DefinirEmailClienteScopeEhValido(this PessoaJuridica pessoaJuridica, Email email)
        {
            return AssertionConcern.IsSatisfiedBy
            (
                AssertionConcern.AssertLength(email.Endereco, Email.EnderecoMinLength, Email.EnderecoMaxLength, "E-mail em tamanho incorreto"),
                AssertionConcern.AssertNotNullOrEmpty(email.Endereco, "O e-mail obrigatória"),
                AssertionConcern.AssertTrue(Email.IsValid(email.Endereco), "E-mail em formato inválido")
            );
        }

        public static bool DefinirCnpjClienteScopeEhValido(this PessoaJuridica pessoaJuridica, Cnpj cnpj)
        {
            return AssertionConcern.IsSatisfiedBy
            (
                AssertionConcern.AssertFixedLength(cnpj.Codigo, Cnpj.ValorMaxCnpj, "CNPJ em tamanho incorreto"),
                AssertionConcern.AssertNotNullOrEmpty(cnpj.GetCnpjCompleto(), "O CNPJ obrigatória"),
                AssertionConcern.AssertTrue(Cpf.Validar(cnpj.Codigo), "CNPJ em formato inválido")
            );
        }
    }
}