﻿using DomainValidation.Interfaces.Validation;
using DomainValidation.Validation;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Interfaces;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Specification;

namespace PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Validation
{
    public class PessoaJuridicaAptoParaCadastroValidation : Validator<Entities.PessoaJuridica>
    {
        public PessoaJuridicaAptoParaCadastroValidation(IPessoaJuridicaRepository repository)
        {
            var cpfDuplicado = new PessoaJuridicaDevePossuirCnpjUnicoSpecification(repository);
            var emailDuplicado = new PessoaJuridicaDevePossuirEmailUnicoSpecification(repository);

            base.Add("cpfDuplicado", new Rule<Entities.PessoaJuridica>(cpfDuplicado, "CPF já cadastrado!"));
            base.Add("emailDuplicado", new Rule<Entities.PessoaJuridica>(emailDuplicado, "E-mail já cadastrado!"));
        }
    }
}