﻿using DomainValidation.Interfaces.Specification;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Interfaces;

namespace PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Specification
{
    public class PessoaJuridicaDevePossuirEmailUnicoSpecification : ISpecification<Entities.PessoaJuridica>
    {
        private readonly IPessoaJuridicaRepository _repository;

        public PessoaJuridicaDevePossuirEmailUnicoSpecification(IPessoaJuridicaRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(Entities.PessoaJuridica pessoa)
        {
            return _repository.ObterPorEmail(pessoa.Email.Endereco) == null;
        }
    }
}