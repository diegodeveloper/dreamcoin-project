﻿using DomainValidation.Interfaces.Specification;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Interfaces;

namespace PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Specification
{
    public class PessoaJuridicaDevePossuirCnpjUnicoSpecification : ISpecification<Entities.PessoaJuridica>
    {
        private readonly IPessoaJuridicaRepository _repository;

        public PessoaJuridicaDevePossuirCnpjUnicoSpecification(IPessoaJuridicaRepository repository)
        {
            _repository = repository;
        }

        public bool IsSatisfiedBy(Entities.PessoaJuridica pessoa)
        {
            return _repository.ObterPorCnpj(pessoa.Cnpj.GetCnpjCompleto()) == null;
        }
    }
}