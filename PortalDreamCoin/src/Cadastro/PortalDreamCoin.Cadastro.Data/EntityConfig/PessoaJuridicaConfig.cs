﻿using System.Data.Entity.ModelConfiguration;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Entities;

namespace PortalDreamCoin.Cadastro.Data.EntityConfig
{
    public class PessoaJuridicaConfig : EntityTypeConfiguration<PessoaJuridica>
    {
        public PessoaJuridicaConfig()
        {
            HasKey(c => new { c.Id });

            Property(c => c.Cnpj.Codigo)
                .HasColumnName("CNPJ");

            Property(c => c.Email.Endereco)
                .HasColumnName("Email");

            Ignore(c => c.ValidationResult);

            ToTable("PessoaJuridica");
        }
    }
}