﻿using System.Data.Entity.ModelConfiguration;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Entities;

namespace PortalDreamCoin.Cadastro.Data.EntityConfig
{
    public class PessoaFisicaConfig : EntityTypeConfiguration<PessoaFisica>
    {
        public PessoaFisicaConfig()
        {
            HasKey(c => new { c.Id });

            Property(c => c.Cpf.Codigo)
                .HasColumnName("CPF");

            Property(c => c.Email.Endereco)
                .HasColumnName("Email");

            Ignore(c => c.ValidationResult);

            ToTable("PessoaFisica");
        }
    }
}