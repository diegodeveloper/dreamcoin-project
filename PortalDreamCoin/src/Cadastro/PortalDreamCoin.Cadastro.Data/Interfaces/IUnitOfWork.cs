﻿using System;

namespace PortalDreamCoin.Cadastro.Data.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
}