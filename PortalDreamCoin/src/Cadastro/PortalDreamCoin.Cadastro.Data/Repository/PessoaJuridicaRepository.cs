﻿using System.Linq;
using PortalDreamCoin.Cadastro.Data.Context;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Entities;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Interfaces;

namespace PortalDreamCoin.Cadastro.Data.Repository
{
    public class PessoaJuridicaRepository : RepositoryBase<PessoaJuridica>, IPessoaJuridicaRepository
    {
        public PessoaJuridicaRepository(CadastroContext db) : base(db)
        {
        }

        public PessoaJuridica ObterPorCnpj(string cnpj)
        {
            return _db.PessoaJuridica.FirstOrDefault(c => c.Cnpj.Codigo == cnpj);
        }

        public PessoaJuridica ObterPorEmail(string email)
        {
            return _db.PessoaJuridica.FirstOrDefault(c => c.Email.Endereco == email);
        }
    }
}