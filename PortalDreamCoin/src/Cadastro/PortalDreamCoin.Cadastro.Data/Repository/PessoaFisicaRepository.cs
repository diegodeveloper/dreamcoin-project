﻿using System.Linq;
using PortalDreamCoin.Cadastro.Data.Context;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Entities;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Interfaces;

namespace PortalDreamCoin.Cadastro.Data.Repository
{
    public class PessoaFisicaRepository : RepositoryBase<PessoaFisica>, IPessoaFisicaRepository
    {
        public PessoaFisicaRepository(CadastroContext db) : base(db)
        {
        }

        public PessoaFisica ObterPorCpf(string cpf)
        {
            return _db.PessoaFisica.FirstOrDefault(c => c.Cpf.Codigo == cpf);
        }

        public PessoaFisica ObterPorEmail(string email)
        {
            return _db.PessoaFisica.FirstOrDefault(c => c.Email.Endereco == email);
        }

    }
}