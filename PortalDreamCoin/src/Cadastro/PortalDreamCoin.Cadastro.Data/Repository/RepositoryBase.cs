﻿using System;
using System.Data.Entity;
using PortalDreamCoin.Cadastro.Data.Context;
using PortalDreamCoin.Cadastro.Domain.Common.Interfaces;

namespace PortalDreamCoin.Cadastro.Data.Repository
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly CadastroContext _db;

        public RepositoryBase(CadastroContext db)
        {
            _db = db;
        }

        public void Adicionar(TEntity obj)
        {
            _db.Set<TEntity>().Add(obj);
        }

        public void Atualizar(TEntity obj)
        {
            var entry = _db.Entry(obj);
            _db.Set<TEntity>().Attach(obj);
            entry.State = EntityState.Modified;
        }

        public TEntity ObterPorId(Guid id)
        {
            return _db.Set<TEntity>().Find(id);
        }
    }
}