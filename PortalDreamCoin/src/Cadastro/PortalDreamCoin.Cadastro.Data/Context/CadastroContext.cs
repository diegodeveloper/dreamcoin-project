﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using PortalDreamCoin.Cadastro.Data.EntityConfig;
using PortalDreamCoin.Cadastro.Domain.PessoaFisica.Entities;
using PortalDreamCoin.Cadastro.Domain.PessoaJuridica.Entities;

namespace PortalDreamCoin.Cadastro.Data.Context
{
    public class CadastroContext : DbContext
    {
        public CadastroContext() : base("DefaultConnection")
        {}

        public DbSet<PessoaFisica> PessoaFisica { get; set; }
        public DbSet<PessoaJuridica> PessoaJuridica { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(100));

            modelBuilder.Configurations.Add(new PessoaFisicaConfig());
            modelBuilder.Configurations.Add(new PessoaJuridicaConfig());

            base.OnModelCreating(modelBuilder);
        }
    }
}