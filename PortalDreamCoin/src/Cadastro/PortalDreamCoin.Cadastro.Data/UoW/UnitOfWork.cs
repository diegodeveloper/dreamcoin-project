﻿using PortalDreamCoin.Cadastro.Data.Context;
using PortalDreamCoin.Cadastro.Data.Interfaces;

namespace PortalDreamCoin.Cadastro.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private CadastroContext _context;

        public UnitOfWork(CadastroContext context)
        {
            _context = context;
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_context != null)
                {
                    _context.Dispose();
                    _context = null;
                }
            }
        }
    }
}