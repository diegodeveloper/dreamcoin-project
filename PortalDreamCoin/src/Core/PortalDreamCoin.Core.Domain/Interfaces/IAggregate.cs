﻿using System;

namespace PortalDreamCoin.Core.Domain.Interfaces
{
    public interface IAggregate
    {
        Guid Id { get; }
    }
}