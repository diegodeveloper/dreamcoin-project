﻿using System;

namespace PortalDreamCoin.Core.Domain.ValuesObjects
{
    public class Aggregate
    {
        public Aggregate()
        {
            this.Id = Guid.NewGuid();
        }

        public Aggregate(Guid id)
        {
            this.Id = id;
        }

        public Guid Id { get; protected set; }
    }
}