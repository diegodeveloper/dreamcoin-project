﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PortalDreamCoin.Site.Startup))]
namespace PortalDreamCoin.Site
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
