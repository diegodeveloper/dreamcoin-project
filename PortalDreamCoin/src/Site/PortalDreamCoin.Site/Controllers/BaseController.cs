﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using PortalDreamCoin.Core.Domain.Events;
using PortalDreamCoin.Core.Domain.Interfaces;

namespace PortalDreamCoin.Site.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        public IHandler<DomainNotification> Notifications;

        public Guid AlunoId
        {
            get
            {
                return Guid.Parse(ControllerContext.HttpContext.User.Identity.GetUserId());
            }
        }

        public BaseController()
        {
            this.Notifications = DomainEvent.Container.GetInstance<IHandler<DomainNotification>>();
        }

        public bool ValidarErrosDominio()
        {
            if (!Notifications.HasNotifications()) return false;

            foreach (var error in Notifications.GetValues())
            {
                ModelState.AddModelError(string.Empty, error.Value);
            }
            return true;
        }
    }
}