﻿using PortalDreamCoin.Core.Domain.ValuesObjects;
using Xunit;

namespace PortalDreamCoin.Core.Test.ValuesObjectsTest.EmailTest
{
    public class EmailTest
    {
        [Fact]
        public void Email_ValidarEmail_True()
        {
            Assert.True(Email.IsValid("email.emailvalido@email.com"));
        }

        [Fact]
        public void Email_ValidarEmail_False()
        {
            Assert.False(Email.IsValid("email.email@invalido@email.com"));
            Assert.False(Email.IsValid("email.emailinvalido.email.com"));
        }
    }
}