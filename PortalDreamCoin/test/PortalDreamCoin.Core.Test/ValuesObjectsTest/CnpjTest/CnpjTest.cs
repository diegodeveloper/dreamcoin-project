﻿using PortalDreamCoin.Core.Domain.ValuesObjects;
using Xunit;

namespace PortalDreamCoin.Core.Test.ValuesObjectsTest.CnpjTest
{
    public class CnpjTest
    {
        [Fact]
        public void Cpf_ValidarCpf_True()
        {
            Assert.True(Cnpj.Validar("96625313000104"));
        }

        [Fact]
        public void Cpf_ValidarCpf_False()
        {
            Assert.False(Cnpj.Validar("986625413000103"));
        }

        [Fact]
        public void Cpf_CompletarCpfInconpleto_True()
        {
            var validacao = new Cnpj("6023566000115");
            var validacao2 = new Cnpj("96625313000104");

            Assert.Equal(14, validacao.GetCnpjCompleto().Length);
            Assert.Equal("06023566000115", validacao.GetCnpjCompleto());

            Assert.Equal(14, validacao2.GetCnpjCompleto().Length);
            Assert.Equal("96625313000104", validacao2.GetCnpjCompleto());
        }

        [Fact]
        public void Cpf_LimparCpfQueIniciaComZero_True()
        {
            Assert.Equal(13, Cnpj.CnpjLimpo("06.023.566/0001-15").Length);
            Assert.Equal("6023566000115", Cnpj.CnpjLimpo("06023566000115"));


            Assert.Equal(14, Cnpj.CnpjLimpo("96.625.313/0001-04").Length);
            Assert.Equal("96625313000104", Cnpj.CnpjLimpo("96625313000104"));
        }
    }
}