﻿using PortalDreamCoin.Core.Domain.ValuesObjects;
using Xunit;

namespace PortalDreamCoin.Core.Test.ValuesObjectsTest.CpfTest
{
    public class CpfTest
    {
        [Fact]
        public void Cpf_ValidarCpf_True()
        {
            Assert.True(Cpf.Validar("01109559275"));
        }

        [Fact]
        public void Cpf_ValidarCpf_False()
        {
            Assert.False(Cpf.Validar("12345678930"));
        }

        [Fact]
        public void Cpf_CompletarCpfInconpleto_True()
        {
            var validacao = new Cpf("1109559275");
            var validacao2 = new Cpf("16204166204");

            Assert.Equal(11, validacao.GetCpfCompleto().Length);
            Assert.Equal("01109559275", validacao.GetCpfCompleto());

            Assert.Equal(11, validacao2.GetCpfCompleto().Length);
            Assert.Equal("16204166204", validacao2.GetCpfCompleto());
        }

        [Fact]
        public void Cpf_LimparCpfQueIniciaComZero_True()
        {
            Assert.Equal(10, Cpf.CpfLimpo("01109559275").Length);
            Assert.Equal("1109559275", Cpf.CpfLimpo("01109559275"));


            Assert.Equal(11, Cpf.CpfLimpo("16204166204").Length);
            Assert.Equal("16204166204", Cpf.CpfLimpo("16204166204"));
        }

    }
}